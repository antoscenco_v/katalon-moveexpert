import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://13:13@moveexpert.stg.tekoway.com/en')

WebUI.click(findTestObject('Account_creation_OR/Home page/Accept cookie'))

WebUI.click(findTestObject('Account_creation_OR/Home page/Register button'))

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/first name'), 'Jhon')

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/last name'), 'Doe')

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/company name'), 'Tekoway')

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/city'), 'Paris')

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/email'), 'tekowaytest1@tekoway.com')

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/password_registration'), 'tekowaytekoway')

WebUI.setText(findTestObject('Account_creation_OR/Registration page/registration form/verify_password_registration'), 'tekowaytekoway')

WebUI.click(findTestObject('Account_creation_OR/Registration page/registration form/Terms of service'))

WebUI.click(findTestObject('Account_creation_OR/Registration page/registration form/Password terms of service'))

WebUI.clickOffset(findTestObject('Account_creation_OR/Registration page/registration form/Page_SmartFerry/div_Recaptcha requires verification Im not a robotreCAPTCHAPrivacy - Terms'), 
    0, 0)

